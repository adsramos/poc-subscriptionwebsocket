package br.com.rd.pocsubscriptionwebsocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocsubscriptionwebsocketApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocsubscriptionwebsocketApplication.class, args);
	}

}
